/**
 *
 */

Meteor.methods({
  sendHandymanProfile: function(doc) {
    doc.insertDoc.id_Handyman = Meteor.userId();
    doc.updateDoc.$set.id_Handyman = Meteor.userId();
    //@DEBUG
    Log.debug(EJSON.stringify(doc));
    //
    check(doc.insertDoc, schemas.handyman_profile);
    if(!doc._id) {
      if(doc.insertDoc.password) {
        Accounts.setPassword(Meteor.userId(), doc.insertDoc.password);
      }
      var emailAlreadyExist = Meteor.users.find({"emails.$.address": doc.insertDoc.email}, {limit: 1}).count() > 0;
      if(!emailAlreadyExist) {
        Meteor.users.update({_id: Meteor.userId()}, {$set:{"emails.0.address": doc.insertDoc.email}});
      }else {
        Meteor.error("Email already exists. You must use other email.");
      }
      return handyman_profile.insert(doc.insertDoc);
    } else {
      handyman_profile.update({_id: doc._id}, doc.updateDoc);
      if(doc.insertDoc.email != Meteor.user().emails[0].address) {
        var emailAlreadyExist = Meteor.users.find({"emails.address": doc.insertDoc.email}, {limit: 1}).count() > 0;
        if(!emailAlreadyExist) {
          Meteor.users.update({_id: Meteor.userId()}, {$set:{"emails.0.address": doc.insertDoc.email}});
        }else {
          Log.error("Email already exists Data context: " + EJSON.stringify(doc) + " User context: " + EJSON.stringify(Meteor.user()));
          throw new Meteor.Error(501, "Email already exists. You must use other email.");
        }
      }
      if(doc.insertDoc.password) {
        Accounts.setPassword(Meteor.userId(), doc.insertDoc.password, {"logout": false});
      }
      return true;
    }
  },
  locationByAddress:function(address){
    var geo = new GeoCoder();
    var result = geo.geocode(address);
    return result;
  },
  addressByLocation: function(location){
    var geo = new GeoCoder();
    var result = geo.reverse(location.lat, location.lng);
    return result;
  },
  updateHandymanLocation: function(location){
    if(!location) {
      return;
    }
    var profile_exists = handyman_profile.findOne({"id_Handyman": Meteor.userId()});
    Log.debug(Meteor.userId());
    if(!profile_exists) {
      var doc = {"id_Handyman": Meteor.userId(),
                 "email": Meteor.user().emails[0]['address'],
                 "currentPosition": {"lat": location.lat, "lng": location.lng}};
      handyman_profile.insert(doc);
    } else {
      handyman_profile.update({_id: profile_exists._id}, {$set:{"currentPosition.lat": location.lat, "currentPosition.lng": location.lng}});
    }
  },
  sendMessageToChat: function(doc) {
    doc.from = Meteor.userId();
    doc.date = new Date();
    //@DEBUG
    Log.debug(EJSON.stringify(doc));
    //
    check(doc, schemas.chat_messages);
    if(!doc._id) {
      var last_id = chat_messages.insert(doc);
      if(last_id) {
        chat.update({_id: doc.id_Chat}, {$inc: {"countMessages": 1}, $set: {"dateLastMessage": new Date()}});
      }
      var to_Client = chat.findOne({_id: doc.id_Chat}, {"fields": {id_Client:1, id_Handyman:1}});

      sendNotification("Chat", "New message", doc.message, to_Client.id_Client, "Chat", to_Client.id_Handyman);

      return last_id;
    } else {
      chat_messages.update({_id: doc._id}, doc);
      return true;
    }
  },
  sendEmail: function(doc) {
    // Important server-side check for security and data integrity
    check(doc, Schema.contact);

    // Build the e-mail text
    var text = "Name: " + doc.name + "\n\n"
            + "Email: " + doc.email + "\n\n\n\n"
            + doc.message;

    this.unblock();

    // Send the e-mail
    Email.send({
        to: Meteor.settings.private.user_admin.email,
        from: doc.email,
        subject: "Website Contact Form - Message From " + doc.name,
        text: text
    });
  },
  changeStatus: function(id, status) {
    var _id_Client = service_request.findOne({_id: id}, {"fields": {id_Client: 1, destinationAddress: 1}});

    return service_request.update({_id: id}, {$set: {status: status}}, function(error, result) {
      if(!error) {
        sendNotification("Status", "Status has been changed", "The request by address \"" + _id_Client.destinationAddress + "\" now is \"" + status + "\"", _id_Client.id_Client, "ListMyRequests");
      }
    });
  },
  makeEstimation: function(estimation) {
    var serviceObj = services.findOne({"_id": estimation.id_Service});
    if(!serviceObj) {
      throw new Meteor.Error("Wrong service Id");
    }

    var requestObj = service_request.findOne({"_id": estimation.id_ServiceRequest});
    if(!requestObj) {
      throw new Meteor.Error("Wrong reqest Id");
    }

    if(invoice.findOne({"id_ServiceRequest": estimation.id_ServiceRequest, $not: {"status": "estimation"}})) {
      throw new Meteor.Error("Request status is not for estimation");
    }

    var obj = {
      "$set": {
        "id_ServiceRequest": estimation.id_ServiceRequest,
        "serviceCost": parseFloat(estimation.hours) * parseFloat(serviceObj.rateHour),
        "transportCost": parseFloat(estimation.distance)/1000 * parseFloat(serviceObj.transportCost),
      }
    };

    obj.$set.total = Math.round((obj.$set.serviceCost + obj.$set.transportCost) * 1000) / 1000;

    //@DEBUG
    Log.debug(EJSON.stringify(obj));
    //

    return invoice.upsert({"id_ServiceRequest": estimation.id_ServiceRequest}, obj);
  },
  deleteNotification: function(id) {
    notification_history.remove({_id: id});
  }
});

var sendNotification = function(from, title, message, userId, path, pathId) {
  var pathId = pathId || "";
  var _handyman_profile = handyman_profile.findOne({id_Handyman: Meteor.userId()}, {"fields": {fullName: 1}});

  var doc = { id_User: userId, message: message, from: _handyman_profile.fullName, path: path, pathId: pathId};
  notification_history.insert(doc, function (error, result) {
			if (!error) {
        Push.send({
          from: from,
          title: title,
          text: message,
          //badge: 12,
          sound: "/audio/alert.mp3",
          query: {
            userId: userId
          }
        });
			}
	});
};
