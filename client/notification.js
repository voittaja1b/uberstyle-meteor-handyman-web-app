bootstrap_alert = function() {};
bootstrap_alert.notification = function(id, message) {
    $('#alert_placeholder').append('<div id="notification_' + id + '" class="alert alert-success alert-dismissible fade in" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+message+'</div>')
    var snd = new Audio('/audio/alert.mp3');
    snd.play();
    delete(snd);
    Meteor.setTimeout(function(){
       $('#alert_placeholder').find('#notification_' + id).remove();
       Meteor.call("deleteNotification", id);
    }, 5000);
};
bootstrap_alert.close = function(id, message) {
    $('#alert_placeholder').find('#notification_' + id).remove();
};
bootstrap_alert.closeAll = function() {
    $('#alert_placeholder').empty();
};
