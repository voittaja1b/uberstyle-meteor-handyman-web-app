/*
 * Hooks for any aims
 */
Meteor.users.after.insert(function (userId, doc) {
  Roles.addUsersToRoles(doc._id, ['handyman'], Roles.GLOBAL_GROUP);
});
