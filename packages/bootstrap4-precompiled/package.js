Package.describe({
  name: 'logvik:bootstrap4-precompiled',
  version: '1.0.1',
  summary: 'Precompiled Bootstrat4 for Meteor',
  git: 'https://github.com/logvik/bootstrap4-wrapper.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {

  api.addFiles('bootstrap4/js/tether.min.js', 'client');
  api.addFiles('bootstrap4/js/bootstrap.min.js', 'client');
  
  api.addAssets([
    'bootstrap4/css/bootstrap.min.css',
	'bootstrap4/css/tether.min.css',
	'bootstrap4/css/tether-theme-arrows.min.css',
	'bootstrap4/css/tether-theme-arrows-dark.min.css',
	'bootstrap4/css/tether-theme-basic.min.css',
  ], 'client');

});
