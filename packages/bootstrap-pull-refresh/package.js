Package.describe({
  name: 'logvik:bootstrap-pull-refresh',
  version: '1.0.1',
  summary: 'Pull refresh (pull down) plugin for infinite scrolling',
  git: 'https://github.com/logvik/bootstrap-pull-refresh.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {
	
  api.addFiles('dist/js/bootstrap.pull-down.js', 'client');
  api.addFiles('dist/css/bootstrap.pull-down.css', 'client');
  
});