Template.registerHelper('isIOS', function () {
  return Platform.isIOS();
});

Template.registerHelper('isAndroid', function () {
  return Platform.isAndroid();
});

Template.registerHelper('isWeb', function () {
  return Platform.isWeb();
});

Template.registerHelper('titleApp', function () {
  return "It24 Handyman";
});

Template.registerHelper('platformClasses', function () {
  if ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web') {
    return 'platform-web';
  }
  if ((Platform.isIOS() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'ios') {
    return 'platform-ios';
  }
  if ((Platform.isAndroid() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'android') {
    return 'platform-android';
  }
  return 'platform-web';
});

Template.registerHelper('curretnPath', function () {
  return Router.current().route.getName();
});

Template.Chat.helpers({
   puredate: function () {
     return moment(this.date).format("dddd, MMMM Do YYYY");
   },
   puretime: function () {
     return moment(this.date).format("HH:mm");
   },
   myMessage: function () {
     return (Meteor.userId() == this.from ? "sent" : "received");
   },
   author: function () {
     var name = "";
     if(Meteor.userId() == this.from) {
       name = "It is my";
     } else {
       var _client_profile = client_profile.findOne({"id_Client": this.from});
       if(_client_profile != null && _client_profile.fullName) {
         name = _client_profile.fullName
       } else {
         name = "Client";
       }
     }
     return name;
   }
});

Template.Chat.onRendered(function () {
  this.find('.messages')._uihooks = {
    insertElement: function (node, next) {
      $(node).insertBefore(next);

      Deps.afterFlush(function() {
        // call width to force the browser to draw it
        //$(node).width();
        var height = 0;
        if(!Session.get("loadingHistory")) {
          height = $(".messages").outerHeight();
        }
        $(".page-content").stop().animate({scrollTop: height}, '500', 'swing');
      });
    }
  }
  if(Router.current().route.getName() === "Chat") {
    myApp7.initPullToRefresh(".page-content");
  }else {
    myApp7.destroyPullToRefresh(".page-content")
  }
  var height = $(".messages").outerHeight();
  $(".page-content").stop().animate({scrollTop: height}, '500', 'swing');
});

Template.ListMyChats.helpers({
   clientName: function () {
     return ((this.client_profile != null && this.client_profile.fullName) ? this.client_profile.fullName : "Some client");
   }
});

Template.ListRequests.helpers({
   clientName: function () {
     return (this.client_profile != null && this.client_profile.fullName ? this.client_profile.fullName : "Some client");
   },
   date: function () {
     return moment(this.dateServicing).format("dddd, MMMM Do YYYY HH:mm");
   },
   formatComment: function () {
     return _.isUndefined(this.comment) ? "No additional comment" : this.comment;
   },
   statusIcon: function () {
     switch(this.status) {
       case "estimation":
         return "fa-money";
         break;
       case "progress":
         return "fa-truck";
         break;
       case "done":
         return "fa-check";
         break;
       case "approved":
         return "fa-check-circle-o";
         break;
      case "closed":
         return "fa-times";
         break;
     }
   },
   selected: function (status) {
     return (this.status == status ? "selected": "");
   },
   disabled: function (status) {
     switch(status) {
       case "estimation":
         return !_.isUndefined(this.invoice) ? "disabled" : "";
         break;
       case "progress":
         return true ? "disabled" : "";
         break;
       case "done":
         return _.isUndefined(this.invoice) ? "disabled" : "";
         break;
       case "approved":
         return true ? "disabled" : "";
         break;
       case "closed":
         return !_.isUndefined(this.invoice) ? "disabled" : "";
         break;
     }
   },
   formatStatus : function() {
     $("div[idRequest='" + this._id + "']").text("");
     return this.status.charAt(0).toUpperCase() + this.status.slice(1);
   },
   enterTime: function() {
     return this.status == "estimation";
   },
   checkInvoice: function(){
     return !_.isUndefined(this.invoice);
   },
   invoiceServiceCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.serviceCost) ? this.invoice.serviceCost : false;
   },
   invoiceTransportCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.transportCost) ? this.invoice.transportCost : false;
   },
   invoiceExtrasCost: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.extrasCost) ? this.invoice.extrasCost : false;
   },
   invoiceTax: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.tax) ? this.invoice.tax : false;
   },
   invoiceTotal: function() {
     return !_.isUndefined(this.invoice) && !_.isUndefined(this.invoice.total) ? this.invoice.total : false;
   }
});

Template.ListRequests.onRendered(function(){
    Geolocation.currentLocation();
});

Template.ListRequests.events({
  'change #status': function (e) {
    Meteor.call("changeStatus", this._id, $("#status").val(), function(error, result) {
      if(error) {
        myApp7.alert(error, "Ups, something happened");
        return;
      } else {
        if(Platform.isAndroid() || Session.get('platformOverride') === 'android') {
          myApp7.addNotification({
              message: 'Saved',
              hold: 5000,
              button: {
                  text: 'Go back',
                  color: 'lightgreen'
              },
          });
        }
        if(Platform.isIOS() || Session.get('platformOverride') === 'ios') {
          myApp7.addNotification({
              title: 'Saved',
              message: 'Click icon to close',
              hold: 5000
          });
        }
      }
    });
  },
  "click #enterTime": function() {
    var requestObj = this;
    var location = Geolocation.currentLocation();
    var error = Geolocation.error();
    if(error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
            myApp7.alert("Device denied the request for Geolocation", "Ups, something happened");
            break;
        case error.POSITION_UNAVAILABLE:
            myApp7.alert("Location information is unavailable.", "Ups, something happened");
            break;
        case error.TIMEOUT:
            myApp7.alert("The request to get device location timed out.", "Ups, something happened");
            break;
        case error.UNKNOWN_ERROR:
            myApp7.alert("An unknown error occurred.", "Ups, something happened");
            break;
        default:
          break;
      }
    }
    if(!location) {
      myApp7.alert("Location information is unavailable. Please, try after few moments.", "Ups, something happened");
      return;
    }
    myApp7.modal({
      title:  'Make estimation',
      text: '<div class="list-block">'+
                    '<ul>'+
                      '<li class="align-top">'+
                          '<div class="item-title full-label">Estimated time in hours</div>'+
                          '<div class="item-input">'+
                            '<input type="number" id="hours"/>'+
                          '</div>'+
                      '</li>'+
                    '</ul>'+
                  '</div>',
      buttons: [
        {
          text: 'Cancel',
          bold: true,
          close: true,
        },
        {
          text: 'Estimate',
          bold: true,
          onClick: function() {
            if(!$("#hours").val()) {
              myApp7.alert("You must enter a hours.", "Ups, something happened");
              return false;
            }
            var distance = geolib.getDistance(
                {latitude: location.coords.latitude, longitude: location.coords.longitude},
                {latitude: requestObj.destinationPoint.lat, longitude: requestObj.destinationPoint.lng}
            );
            var estimation = {
              id_Service: requestObj.id_Service,
              id_ServiceRequest: requestObj._id,
              hours : $("#hours").val(),
              distance: distance
            };
            Meteor.call("makeEstimation", estimation, function(error, result){
              if(error){
                console.log("error", error);
                myApp7.alert(error, "Ups, something happened");
              }
              if(result){
                if(Platform.isAndroid() || Session.get('platformOverride') === 'android') {
                  myApp7.addNotification({
                      message: 'Estimation have been sent to client',
                      hold: 5000,
                      button: {
                          text: 'Go back',
                          color: 'lightgreen'
                      },
                  });
                }
                if(Platform.isIOS() || Session.get('platformOverride') === 'ios') {
                  myApp7.addNotification({
                      title: 'Estimation have been sent to client',
                      message: 'Click icon to close',
                      hold: 5000
                  });
                }
              }
            });
          }
        },
      ]
    });
  }
});

/*
 * {{> PDRender name='customTemplate' data=.}}
 * Will check existing web_customTemplate, ios_customTemplate, android_customTemplate on working device
 */
UI.registerHelper('PDRender', function () {
    var component;
    if ((Meteor.isClient && Platform.isWeb()) || Session.get('platformOverride') === 'web') {
        component = Template["web_"+this.name];
        if (!_.isUndefined(component)) {
          return Blaze._getTemplate("web_"+this.name, function () {
            return Template.instance();
          });
        } else {
          return Blaze._getTemplate(this.name, function () {
            return Template.instance();
          });
        }
    }
    if ((Platform.isIOS()) || Session.get('platformOverride') === 'ios') {
        component = Template["ios_"+this.name];
        if (!_.isUndefined(component)) {
          return Blaze._getTemplate("ios_"+this.name, function () {
            return Template.instance();
          });
        } else {
          return Blaze._getTemplate(this.name, function () {
            return Template.instance();
          });
        }
    }
    if ((Platform.isAndroid()) || Session.get('platformOverride') === 'android') {
        component = Template["android_"+this.name];
        if (!_.isUndefined(component)) {
          return Blaze._getTemplate("android_"+this.name, function () {
            return Template.instance();
          });
        } else {
          return Blaze._getTemplate(this.name, function () {
            return Template.instance();
          });
        }
    }
    return UI.Component;
});

UI.registerHelper("formTypeMethod", function(){
  if(_.isEmpty(this)) {
    return 'method'
  } else {
    return 'method-update';
  }
});

/**
 * @DEPRECATED

Session.setDefault('counter', 0);

Template.Home.helpers({
  counter: function () {
    return Session.get('counter');
  }
})

Template.Home.events({
  'click button': function () {
    // increment the counter when button is clicked
    Session.set('counter', Session.get('counter') + 1);
  }
});
 */
 Template.LeftMenu.helpers({
     active: function(route) {
         return Router.current().route.getName() == route ? 'list-group-item-info' : '';
     }
 });

Template.SignIn.events({
    'mouseDown #login-buttons-google': function () {
        $("#login-buttons-google").addClass('active-state');
    },
    'mouseUp #login-buttons-google': function () {
        $("#login-buttons-google").removeClass('active-state');
    },
    'mouseDown #login-buttons-twitter': function () {
        $("#login-buttons-twitter").addClass('active-state');
    },
    'mouseUp #login-buttons-twitter': function () {
        $("#login-buttons-twitter").removeClass('active-state');
    },
    'mouseDown #login-buttons-facebook': function () {
        $("#login-buttons-facebook").addClass('active-state');
    },
    'mouseUp #login-buttons-facebook': function () {
        $("#login-buttons-facebook").removeClass('active-state');
    },
    'mouseDown #login-buttons-password': function () {
        $("#login-buttons-password").addClass('active-state');
    },
    'mouseUp #login-buttons-password': function () {
        $("#login-buttons-password").removeClass('active-state');
    }
});

Template.extendOnPlatforms(['android','ios','web'], 'SignIn');

Template.HandymanProfileForm.helpers({
    HandymanProfileFormSchema: function() {
        return schemas.handyman_profile;
    },
});

Template.HandymanProfileForm.events({
    'click #reset': function () {
        Router.go("/");
    },
    'click #submit': function () {
        if(!AutoForm.validateForm("HandymanProfileForm")) {
          preventDefault();
        }
        var doc = AutoForm.getFormValues("HandymanProfileForm");
        doc._id = $("#docid").val();
        Meteor.call("sendHandymanProfile", doc, function(error, result) {
          if(error) {
            myApp7.alert(error, "Ups, something happened");
            return;
          }

          if(result) {
            if(Platform.isAndroid() || Session.get('platformOverride') === 'android') {
              myApp7.addNotification({
                  message: 'Saved',
                  hold: 5000,
                  button: {
                      text: 'Go back',
                      color: 'lightgreen'
                  }
              });
            }
            if(Platform.isIOS() || Session.get('platformOverride') === 'ios') {
              myApp7.addNotification({
                  title: 'Saved',
                  message: 'Click icon to close',
                  hold: 5000
              });
            }
          }
        });
    }
    //save by method
});

var onRenderedHandymanMap = function(){
    AdaptiveMaps.init({
        'sensor': true, //optional
        'key': (Meteor.settings.public && Meteor.settings.public.google_maps ? Meteor.settings.public.google_maps: "")
        //'language': 'es' //optional
    }, function () {
       if(!location) {
         myApp7.alert("Our App could not define your coordinates, please check your settings of a restrictions for a geolocation.", "Ups, something happened");
       }
       var center_adaptiveMap = !_.isEmpty(globalDict.get("current_center_adaptiveMap")) ? new AdaptiveMaps.LatLng(globalDict.get("current_center_adaptiveMap").lat, globalDict.get("current_center_adaptiveMap").lng) : new AdaptiveMaps.LatLng(40.735877, -73.990394);
       var mapOptions = {
           mapTypeId: 'ROADMAP',
           camera: {
               latLng: center_adaptiveMap,
               zoom: 12
           }
       }

       adaptiveMap = new AdaptiveMaps.Map(document.getElementById('map'), mapOptions);

       /*
       * Here we handle center_changed event and store in dictionary
       */
       adaptiveMap.addEventListener("center_changed", function() {
         var center = adaptiveMap.getCenter();
         globalDict.set("current_center_adaptiveMap", {"lat": center.lat(), "lng": center.lng()});
       });
       /*
       adaptiveMap.addLiveMarkers({
    			cursor: handyman_profile.find(),
     			transform: function (profile) {
     				return {
     					title: profile.fullName,
     					position: new AdaptiveMaps.LatLng(profile.currentPosition.lat, currentPosition.lon),
     					profile: profile,
              icon: '/icon/handyman.png',
              // This marker is 20 pixels wide by 32 pixels high.
              size: new google.maps.Size(32, 32),
              // The origin for this image is (0, 0).
              origin: new google.maps.Point(0, 0),
              // The anchor for this image is the base of the flagpole at (0, 32).
              anchor: new google.maps.Point(0, 32),
              infoWindowAnchor: [0,0]
     				}
     			},
     			onClick: function () {
     				console.log(profile);
     			}
     	 });
       */
    });

    //Init typeWatch plugin for address searching
    $("#getAddressGeocoder").typeWatch( {
        callback: function (value) {
          $("#getAddressGeocoder").trigger('typeWatch', this);
        },
        wait: 750,
        captureLength: 2
    } );

    myApp7.closePanel();
};

Template.HandymanMap.onRendered(onRenderedHandymanMap);
Template.HandymanMapBlock.onRendered(onRenderedHandymanMap);

Template.Support.helpers({
  contactFormSchema: function() {
    return schemas.contact;
  }
});

Template.RightMenu.helpers({
  Title: function() {
    if(Router.current().route.options.title) {
      return Router.current().route.options.title;
    } else {
        return false;
    }
  },
  showTitle: function() {
    return _.indexOf(["Chat", "HandymanMap"], Router.current().route.getName() ) === -1;
  },
  toolbarMessage: function() {
    return Router.current().route.getName() === "Chat";
  }
});

for(var property in Template){
  // check if the property is actually a blaze template
  if(Blaze.isTemplate(Template[property])){
    var template=Template[property];
    // assign the template an onRendered callback who simply prints the view name
    template.onRendered(function(){
      myApp7 = new Framework7({
          router: false,
          //swipePanel: 'left',
          swipeBackPage: true,
          animatePages: true
      });
      myApp7.closePanel();
    });
  }
}

Template.extendOnPlatforms(['android','ios','web'], 'LeftMenu');
Template.extendOnPlatforms(['android','ios','web'], 'HandymanProfileForm');
Template.extendOnPlatforms(['android','ios','web'], 'RightMenu');
Template.extendOnPlatforms(['android','ios','web'], 'Support');
Template.extendOnPlatforms(['android','ios','web'], 'ListRequests');
Template.extendOnPlatforms(['android','ios','web'], 'ListMyChats');
Template.extendOnPlatforms(['android','ios','web'], 'Chat');

Template.web_Chat.helpers({
   puredate: function () {
     return moment(this.date).format("dddd, MMMM Do YYYY");
   },
   puretime: function () {
     return moment(this.date).format("HH:mm");
   },
   myMessage: function () {
     return (Meteor.userId() == this.from ? "right" : "left");
   },
   author: function () {
     var name = "";
     if(Meteor.userId() == this.from) {
       name = "It is my";
     } else {
       var _client_profile = client_profile.findOne({"id_Client": this.from});
       if(_client_profile != null && _client_profile.fullName) {
         name = _client_profile.fullName
       } else {
         name = "Client";
       }
     }
     return name;
   }
});

Template.web_Chat.events({
  "click #loadHistory": function() {
    $(".page-content").trigger("refresh");
  }
});

Template.web_Chat.onRendered(function () {

  this.find('.chat')._uihooks = {
    insertElement: function (node, next) {
      $(node).insertBefore(next);

      Deps.afterFlush(function() {
        // call width to force the browser to draw it
        //$(node).width();
        var height = 0;
        if(!Session.get("loadingHistory")) {
          height = $(".chat").outerHeight();
        }
        $(".messages").stop().animate({scrollTop: height}, '500', 'swing');
      });
    }
  }
  var height = $(".chat").outerHeight();

  Push.addListener('message', function(notification) {
    console.log(notification);
  });

  $(".messages").stop().animate({scrollTop: height}, '500', 'swing');
});
