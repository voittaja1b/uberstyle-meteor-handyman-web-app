/**
 * Security on access to documents in MongoDb
 */
Security.defineMethod('ownsDocument', {
  fetch: ['ownerId'],
  deny: function (type, arg, userId, doc) {
    return userId !== doc.ownerId;
  }
});

client_profile.permit('insert').ifHasRole('client').apply();
client_profile.permit('insert').ifHasRole('admin').apply();
client_profile.permit('update').ownsDocument().apply();
client_profile.permit('update').ifHasRole('admin').apply();

handyman_profile.permit('insert').ifHasRole('client').apply();
handyman_profile.permit('insert').ifHasRole('admin').apply();
handyman_profile.permit('update').ownsDocument().apply();
handyman_profile.permit('update').ifHasRole('admin').apply();

Push.allow({
    send: function(userId, notification) {
      // Allow all users to send to everybody - For test only!
      return true;
    }
});
