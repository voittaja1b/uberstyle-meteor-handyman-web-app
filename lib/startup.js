// code to run on server at startup
Meteor.startup(function () {

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        Iron.utils.debug = true;
    }

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        Push.debug = true;
    }

    /**
     ** Settings for Logger
     */
    Log = new Logger();

    if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
      new LoggerConsole(Log, {}).enable({
        enable: true,
        filter: ['*'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
        client: true, /* This allows to call, but not execute on Client */
        server: true   /* Calls from client will be executed on Server */
      });
    }

    new LoggerMongo(Log, {}).enable({
      enable: true,
      filter: ['ERROR', 'FATAL', 'WARN'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
      client: false, /* This allows to call, but not execute on Client */
      server: true   /* Calls from client will be executed on Server */
    });

    new LoggerRollbar(Log, {}).enable({
      enable: true,
      filter: ['*'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
      client: true, /* This allows to call, but not execute on Client */
      server: true   /* Calls from client will be executed on Server */
    });
    
    if (Meteor.isClient) {
        if(typeof(Meteor.settings.public.platformOverride) != "undefined" && Meteor.settings.public.platformOverride) {
            Session.set("platformOverride", Meteor.settings.public.platformOverride);
        }

        if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
            AutoForm.debug();
        }
    }

    //Section for running only on server
    if (Meteor.isServer) {
      Accounts.emailTemplates.siteName = Meteor.settings.public.sitename;
      Accounts.emailTemplates.from = Meteor.settings.private.email.from;

      Mailer.config({
          from: Meteor.settings.private.email.from,     // Default 'From:' address. Required.
          replyTo: Meteor.settings.private.email.replyTo,  // Defaults to `from`.
          routePrefix: 'emails',              // Route prefix.
          baseUrl: process.env.ROOT_URL,      // The base domain to build absolute link URLs from in the emails.
          testEmail: null,                    // Default address to send test emails to.
          logger: Log,                 // Injected logger (see further below)
          silent: false,                      // If set to `true`, any `Logger.info` calls won't be shown in the console to reduce clutter.
          addRoutes: Meteor.settings.public.debug, // Add routes for previewing and sending emails. Defaults to `true` in development.
          language: 'html',                    // The template language to use. Defaults to 'html', but can be anything Meteor SSR supports (like Jade, for instance).
          plainText: true,                     // Send plain text version of HTML email as well.
          plainTextOpts: {}                   // Options for `html-to-text` module. See all here: https://www.npmjs.com/package/html-to-text
      });

      /**
      * Debug of SimpleSchema
      */
      if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
        SimpleSchema.debug = true
      }
    }
});
