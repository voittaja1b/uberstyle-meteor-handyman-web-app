/**
* Publisher function will send doses of information to client
*/

Meteor.publish("roles", function (){
  return Meteor.roles.find({});
});
Meteor.publish("notification", function (){
  return notification_history.find({"id_User": this.userId});
});
Meteor.publish('profile', function() {
  return handyman_profile.find({"id_Handyman": this.userId});
});

Meteor.publish('requests', function(location) {
  //here we must use algorithm for definition of circle area around of user location ~ 40km
  var params = {};
  if(location) {
    params["destinationPoint"] = { $geoWithin: { $centerSphere: [ [location.lat, location.lng] , (40/6378.1) ] } };
    return service_request.find(params);
  } else {
    return service_request.find({_id: 1});
  }
});

Meteor.publishTransformed('listmychats', function(limit) {
  var _limit = limit || 50;
  return chat.find({"id_Handyman": this.userId}, {"limit": _limit, "sort": {"dateLastMessage": -1}}).serverTransform({
    // we extending the document with the custom property 'client_profile'
    client_profile: function(doc) {
      return client_profile.findOne({
        "id_Client": doc.id_Client
      });
    }
  });
});

Meteor.publish('chat', function(id_Client, id_Handyman, limit) {
  var _limit = limit || 50;
  var _chat = chat.find({"id_Client": id_Client, "id_Handyman": id_Handyman});
  if(_chat.fetch().length == 0) {
    chat.insert({"id_Client": id_Client, "id_Handyman": id_Handyman, "dateLastMessage": new Date()});
    _chat = chat.find({"id_Client": id_Client, "id_Handyman": id_Handyman});
  }
  _client_profile = client_profile.find({"id_Client": id_Client});
  return [_chat, _client_profile, chat_messages.find({"id_Chat": _chat.fetch()[0]._id}, {"limit" : _limit, "sort": {"date": -1}})];
});

Meteor.publish('chatHistory', function(_id_Chat, limit) {
  var _limit = parseInt(limit) || 50;
  return chat_messages.find({"id_Chat": _id_Chat}, {"limit" : _limit, "sort": {"date": -1}});
});

Meteor.publishTransformed('listrequests', function(limit) {
  var _limit = limit || 50;
  return service_request.find({"id_Handyman": this.userId}, {"limit": _limit, "sort": {"dateServicing": -1}}).serverTransform({
    // we extending the document with the custom property 'handyman_profile'
    client_profile: function(doc) {
      return client_profile.findOne({
        "id_Client": doc.id_Client
      });
    },
    service: function (doc) {
      return services.findOne({
        "_id": doc.id_Service
      });
    },
    invoice: function (doc) {
      return invoice.findOne({
        "id_ServiceRequest": doc._id
      });
    }
  });
});

Meteor.publish('request', function(id) {
  return service_request.find({"_id": id, "id_Handyman": this.userId});
});
