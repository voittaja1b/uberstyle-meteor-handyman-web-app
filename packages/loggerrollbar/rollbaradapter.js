/**
* @file Contain definition the adapter Rollbar for ostrio logger
*/

LoggerRollbar = (function() {
  function LoggerRollbar(logger) {
    this.logger = logger;
    check(logger, Match.OneOf(Logger, Object));
    logger.add('Rollbar', function(level, message, data, userId) {
      data.userId = userId;
      throwError(message, data, level);
    }, function() {}, true);
  }

  LoggerRollbar.prototype.enable = function(rule) {
    if (rule == null) {
      rule = {};
    }
    check(rule, Object);
    if (rule.enable == null) {
      rule.enable = true;
    }
    if (rule.client == null) {
      rule.client = false;
    }
    if (rule.server == null) {
      rule.server = true;
    }
    this.logger.rule('Rollbar', rule);
    return this;
  };

  return LoggerRollbar;

})();
