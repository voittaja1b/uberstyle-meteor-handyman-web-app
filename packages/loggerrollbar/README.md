### Rollbar Logger

## Add package

```
meteor add logvik:loggerrollbar
```

## Configuration


Set 'ROLLBAR_SERVER_ACCESS_TOKEN', and optionally 'ROLLBAR_CLIENT_ACCESS_TOKEN' (if you want to enable the browserJS rollbar reporter) and also optionally, the 'ROLLBAR_ENVIRONMENT'environment variables with your client and server access tokens for rollbar
Run meteor

```
$ ROLLBAR_SERVER_ACCESS_TOKEN=acoken1 \
 ROLLBAR_CLIENT_ACCESS_TOKEN=acefe \
 ROLLBAR_ENVIRONMENT=devel \
 meteor
```

## How to use it?

Enable Adapter

```
Log = new Logger();
new LoggerRollbar(Log, {}).enable({
  enable: true,
  filter: ['*'], /* Filters: 'ERROR', 'FATAL', 'WARN', 'DEBUG', 'INFO', 'TRACE', '*' */
  client: true, /* This allows to call, but not execute on Client */
  server: true   /* Calls from client will be executed on Server */
});
```

Logging
```
Log.info(message, data, userId);
Log.debug(message, data, userId);
Log.error(message, data, userId);
Log.fatal(message, data, userId);
Log.warn(message, data, userId);
```

## Additional documentation

1. ostrio:logger [click here](https://github.com/VeliovGroup/Meteor-logger/)
2. saucecode:rollbar [click here](https://github.com/thesaucecode/meteor-rollbar)
3. npm rollbar [click here](https://rollbar.com/docs/notifier/node_rollbar/)
