App.info({
  name: 'Handyman It24',
  description: 'Handyman It24 app.',
  author: 'Konstantin Viktorov',
  email: 'e@logvik.com',
  website: 'http://it24.es',
  version: '0.0.1'
});

App.icons({
  // iOS
  'iphone': 'private/icons/icon-60x60.png',
  'iphone_2x': 'private/icons/icon-60x60@2x.png',
  'ipad': 'private/icons/icon-76x76.png',
  'ipad_2x': 'private/icons/icon-76x76@2x.png',

  // Android
  'android_ldpi': 'private/icons/icon-36x36.png',
  'android_mdpi': 'private/icons/icon-48x48.png',
  'android_hdpi': 'private/icons/icon-72x72.png',
  'android_xhdpi': 'private/icons/icon-96x96.png'
});

App.launchScreens({
  // iOS
  'iphone': 'private/splash/splash-320x480.png',
  'iphone_2x': 'private/splash/splash-320x480@2x.png',
  'iphone5': 'private/splash/splash-320x568@2x.png',
  'ipad_portrait': 'private/splash/splash-768x1024.png',
  'ipad_portrait_2x': 'private/splash/splash-768x1024@2x.png',
  'ipad_landscape': 'private/splash/splash-1024x768.png',
  'ipad_landscape_2x': 'private/splash/splash-1024x768@2x.png',

  // Android
  'android_ldpi_portrait': 'private/splash/splash-200x320.png',
  'android_ldpi_landscape': 'private/splash/splash-320x200.png',
  'android_mdpi_portrait': 'private/splash/splash-320x480.png',
  'android_mdpi_landscape': 'private/splash/splash-480x320.png',
  'android_hdpi_portrait': 'private/splash/splash-480x800.png',
  'android_hdpi_landscape': 'private/splash/splash-800x480.png',
  'android_xhdpi_portrait': 'private/splash/splash-720x1280.png',
  'android_xhdpi_landscape': 'private/splash/splash-1280x720.png'
});

App.setPreference('StatusBarOverlaysWebView', 'false');
App.setPreference('StatusBarBackgroundColor', '#000000');

