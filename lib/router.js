Router.setTemplateNameConverter(function (str) {
    var component;
    str = Iron.utils.classCase(str);
    if ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web') {
        if (Template["web_"+str]) {
          return "web_"+str;
        }
    }
    if ((Meteor.isCordova && Platform.isIOS() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'ios') {
        if (Template["ios_"+str]) {
          return "ios_"+str;
        }
    }
    if ((Meteor.isCordova && Platform.isAndroid() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'android') {
        if (Template["android_"+str]) {
          return "android_"+str;
        }
    }
    return str;
});


ApplicationController = RouteController.extend({
    layoutTemplate: 'ApplicationLayout',
    onStop: function(){
      // register the previous route location in a session variable
      Session.set("previousLocationPath", Router.current().route.path());
    }
});

Router.configure({
    controller: "ApplicationController",
    notFoundTemplate: "NotFound",
    loadingTemplate: "Loading"
});

AccountsTemplates.configure({
    defaultLayout: 'AccountsLayout',
    enablePasswordChange: false,
    sendVerificationEmail: true,
    showResendVerificationEmailLink: false,
    showForgotPasswordLink: true,
    overrideLoginErrors: false,
    continuousValidation: true,
    showValidating: true,
    negativeFeedback: true,
    showLabels: false,
    //showReCaptcha: true,
});

/*
 * All templates helpers which we can use
 * Here https://github.com/meteor/meteor/tree/master/packages/accounts-ui-unstyled
 * https://github.com/meteor-useraccounts/core/blob/master/Guide.md
 */
/*
change password changePwd       atChangePwd     /change-password        fullPageAtForm
enroll account  enrollAccount   atEnrollAccount /enroll-account fullPageAtForm  X
 */
AccountsTemplates.configureRoute('signIn', {
    name: 'SignIn',
    path: '/login',
    redirect: '/profile',
    template: 'SignIn',
});

AccountsTemplates.configureRoute('signUp', {
    name: 'SignUp',
    path: '/register',
    redirect: '/',
    template: 'SignUp',
});

AccountsTemplates.configureRoute('forgotPwd', {
    name: 'ForgotPwd',
    path: '/forgot',
    redirect: '/login',
    template: 'ForgotPwd',
});

AccountsTemplates.configureRoute('verifyEmail', {
    name: 'VerifyEmail',
    path: '/verify-email',
    redirect: '/profile',
    template: 'VerifyEmail',
});

AccountsTemplates.configureRoute('resetPwd', {
    name: 'ResetPwd',
    path: '/reset-password',
    redirect: '/profile',
    template: 'ResetPwd',
});

AccountsTemplates.configure({
    onLogoutHook: function() {
        //redirect after logout
        Router.go('/login');
    }
});

/*
Router.plugin('ensureSignedIn', {
    except: _.pluck(AccountsTemplates.routes, 'name')
    //.concat(['home', 'contacts'])
});
*/
HandymanAreaController = ApplicationController.extend({
    waitOn: function () {
        return [ Meteor.subscribe("roles"), Meteor.subscribe("notification") ];
    },
    onBeforeAction: function () {
        if (!Roles.userIsInRole(Meteor.userId(), ['handyman'])) {
          Router.go('/login');
        } else {
          this.next();
        }
    }
});

ApplicationMapController = HandymanAreaController.extend({
    subscriptions: function() {
      return [Meteor.subscribe('requests')];
    },
    action : function () {
      if (this.ready()) {
        this.render();
      } else {
        this.render('Loading');
      }
    }
});

Router.map(function() {
    this.route('HandymanMap', {
        title: 'HandymanMap',
        path: '/',
        controller: "ApplicationMapController",
    });
});

Router.map(function() {
    this.route('HandymanProfileForm', {
        title: 'Profile',
        path: '/profile',
        controller: 'HandymanAreaController',
        data: function () {
            return handyman_profile.findOne({"id_Handyman": Meteor.userId()});
        },
        waitOn: function() {
          return Meteor.subscribe('profile');
        }
    });
});


Router.map(function() {
    this.route('ListMyChats', {
        title: 'List of my chats',
        path: '/list-my-chats',
        controller: 'HandymanAreaController',
        data: function () {
          return {"listmychats": chat.find()};
        },
        waitOn: function() {
          return Meteor.subscribe('listmychats');
        }
    });
});

ApplicationChatController = HandymanAreaController.extend({
    data: function () {
      var chat_id = chat.findOne({"id_Handyman": Meteor.userId(), "id_Client": this.params._id});
      if(!_.isUndefined(chat_id)) {
        return {"chat_id": chat_id,
                "messages": chat_messages.find({"id_Chat": chat_id._id}, {"sort": {"date" : 1}})};
      }else {
        return {};
      }
    },
    waitOn: function() {
      return Meteor.subscribe('chat', this.params._id, Meteor.userId());
    }
});

Router.map(function() {
    this.route('Chat', {
        title: 'Chat',
        path: '/chat/:_id',
        controller: 'ApplicationChatController'
    });
});


Router.map(function() {
    this.route('ListRequests', {
        title: 'List of requests',
        path: '/list-requests',
        controller: 'HandymanAreaController',
        data: function () {
          return {"listrequests": service_request.find()};
        },
        waitOn: function() {
          return Meteor.subscribe('listrequests');
        }
    });
});

Router.map(function() {
    this.route('PaymentForm', {
        title: 'Payment',
        path: '/payment',
        controller: 'HandymanAreaController',
    });
});

Router.map(function() {
    this.route('Support', {
        title: 'Support',
        path: '/support',
        controller: 'HandymanAreaController',
        data: function () {
            return handyman_profile.findOne({"id_Handyman": Meteor.userId()});
        },
        waitOn: function() {
          return Meteor.subscribe('profile');
        }
    });
});

Router.map(function() {
    this.route('About', {
        title: 'About service',
        path: '/about',
        controller: 'HandymanAreaController',
    });
});
