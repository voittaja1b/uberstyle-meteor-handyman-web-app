/**
 * All structure of data here
 * @URL https://github.com/aldeed/meteor-collection2
 */

schemas = {};

//(id:string, name:String, fullName:String, email:String, phone:String)
schemas.client_profile = new SimpleSchema({
    id_Client: {
        type: String,
        label: "Id client",
        max: 200,
        unique: true
    },
    fullName: {
        type: String,
        label: "Full Name",
        optional: true
    },
    email: {
        type: String,
        label: "Email",
        regEx: SimpleSchema.RegEx.Email,
        index: true,
        unique: true,
        defaultValue: function() {
          if(Meteor.user()) {
            return Meteor.user().emails[0]['address'];
          }
        }
    },
    password: {
        type: String,
        label: "Password",
        optional: true,
        autoValue: function() {
            // Prevent user from supplying their own value, password will be stored in another place
            this.unset();
        }
    },
    phone: {
        type: String,
        label: "Phone",
        optional: true,
        regEx: /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{3,25}$/
    },
    cardnumber: {
        type: String,
        label: "Card #",
        optional: true,
        regEx: /^\d{4} \d{4} \d{4} \d{4}$/,
        autoValue: function() {
            // Prevent user from supplying their own value, we can not save card number. We send this info for creating account on processing service.
            this.unset();
        }
    },
    cvv: {
        type: String,
        label: "CVV",
        optional: true,
        min: 3,
        max: 3,
        autoValue: function() {
            // Prevent user from supplying their own value, we can not save card cvv. We send this info for creating account on processing service.
            this.unset();
        }
    },
    expired: {
        type: String,
        label: "Expired",
        optional: true,
        regEx: /^\d{2}\/\d{2}$/,
        autoValue: function() {
            // Prevent user from supplying their own value, we can not save card expired. We send this info for creating account on processing service.
            this.unset();
        }
    },
});

client_profile.attachSchema(schemas.client_profile);

//(id:string, name:String, fullName:String, email:String, phone:String, rating: Float, countFeedbacks:Int, availability:[timeFrom,timeTo], currentPosition:point{lat:Float,lng:Float})
schemas.handyman_profile = new SimpleSchema({
    id_Handyman: {
        type: String,
        label: "Id Handyman",
        max: 200
    },
    password: {
        type: String,
        label: "Password",
        optional: true,
        autoValue: function() {
            // Prevent user from supplying their own value, password will be stored in another place
            this.unset();
        }
    },
    fullName: {
        type: String,
        label: "Full Name",
        optional: true
    },
    email: {
        type: String,
        label: "Email",
        regEx: SimpleSchema.RegEx.Email,
        index: true,
        unique: true,
        defaultValue: function() {
          if(Meteor.user()) {
            return Meteor.user().emails[0]['address'];
          }
        }
    },
    phone: {
        type: String,
        label: "Phone",
        optional: true,
        regEx: /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{3,25}$/
    },
    rating: {
        type: Number,
        label: "Rating",
        optional: true,
        decimal: true,
        defaultValue: 0,
        min:0,
        max:5
    },
    countFeedbacks: {
        type: Number,
        label: "Count feedbacks",
        defaultValue: 0,
        optional: true,
    },
    availability: {
        type: Object,
        label: "Hours of availability",
        optional: true
    },
    "availability.from": {
        type: Number,
        label: "Availability from",
        allowedValues: _.range(0, 1440, 30),
        autoform: {
          options: function () {
            return _.map(_.range(0, 1440, 30) , function (v, i) {
              var hr  = new String(Math.floor(v/60));
              var min = new String(v%60);
              return {label: (hr.length == 1 ? "0" + hr : hr) + ":" + (min.length == 1 ? "0" + min : min) , value: v};
            });
          }
        }
    },
    "availability.to": {
        type: Number,
        label: "Availability to",
        allowedValues: _.range(0, 1440, 30),
        autoform: {
          options: function () {
            return _.map(_.range(0, 1440, 30) , function (v, i) {
              var hr  = new String(Math.floor(v/60));
              var min = new String(v%60);
              return {label: (hr.length == 1 ? "0" + hr : hr) + ":" + (min.length == 1 ? "0" + min : min) , value: v};
            });
          }
        }
    },
    currentPosition: {
        type: Object,
        label: "Current position",
        optional: true
    },
    "currentPosition.lat": {
        type: Number,
        label: "Latitude",
        decimal: true
    },
    "currentPosition.lng": {
        type: Number,
        label: "Longitude",
        decimal: true
    },
    distance: {
        type: Number,
        label: "Temporary distance",
        defaultValue: 0,
        optional: true
    },
});

handyman_profile.attachSchema(schemas.handyman_profile);

//(id:string, id_Handyman:String, status:String[busy,free])
schemas.handyman_availability = new SimpleSchema({
    id_Handyman: {
        type: String,
        label: "Id handyman",
        max: 200
    },
    status: {
        type: Number,
        label: "Status (0-free, 1-busy)",
        allowedValues: [0,1]
    }
});

handyman_availability.attachSchema(schemas.handyman_availability);

//(id:string, name:String, transportCost:Float, rateHour:Float)
schemas.services = new SimpleSchema({
    name: {
        type: String,
        label: "Name",
        max: 200
    },
    transportCost: {
        type: Number,
        label: "Transport cost",
        optional: true,
        decimal: true
    },
    rateHour: {
        type: Number,
        label: "Rate hour",
        optional: true,
        decimal: true
    },
});

services.attachSchema(schemas.services);

//(id:String, id_Client:String, id_Handyman:String, destinationPoint:point{lat:Float,lng:Float}, id_Service:String, comment:String, status:enum[estimation,progress,done,approved,closed])
schemas.service_request = new SimpleSchema({
    id_Client: {
        type: String,
        label: "Id client",
        max: 200
    },
    id_Handyman: {
        type: String,
        label: "Id handyman",
        max: 200,
        optional: true,
    },
    destinationAddress: {
        type: String,
        label: "Address",
        optional: true,
    },
    destinationPoint: {
        type: Object,
        label: "Destination point",
        optional: true,
    },
    "destinationPoint.lat": {
        type: Number,
        label: "Latitude",
        decimal: true
    },
    "destinationPoint.lng": {
        type: Number,
        label: "Longitude",
        decimal: true
    },
    id_Service: {
        type: String,
        label: "Id service",
        max: 200
    },
    dateServicing: {
        type: Date,
        label: "Date of servicing",
        optional: true,
        defaultValue: new Date()
    },
    comment: {
        type: String,
        label: "Comment",
        optional: true,
        max: 1000
    },
    status: {
        type: String,
        label: "Status",
        allowedValues: ['estimation', 'progress', 'done', 'approved', 'closed']
    },
});

service_request.attachSchema(schemas.service_request);

//(id:String, id_ServiceRequest:String, serviceCost:Float, transportCost:Float, extrasCost:Float, total:Int, status:enum[unpaid,paid,completed])
schemas.invoice = new SimpleSchema({
    id_ServiceRequest: {
        type: String,
        label: "Id service request",
        max: 200
    },
    serviceCost: {
        type: Number,
        label: "Service cost",
        decimal: true
    },
    transportCost: {
        type: Number,
        label: "Transport cost",
        optional: true,
        decimal: true
    },
    extrasCost: {
        type: Number,
        label: "Extras cost",
        optional: true,
        decimal: true
    },
    tax: {
        type: Number,
        label: "Tax (VAT)",
        optional: true,
        decimal: true
    },
    total: {
        type: Number,
        label: "Total",
        decimal: true
    },
    status: {
        type: String,
        label: "Status",
        optional: true,
        allowedValues: ['estimation', 'unpaid', 'paid', 'completed'],
        defaultValue: function() {
          return 'estimation';
        }
    },
});

invoice.attachSchema(schemas.invoice);

//(id:String, id_Invoice:String, transactionId:Int, sum:Float, status:enum[init,done], message:String)
schemas.payments = new SimpleSchema({
    id_Invoice: {
        type: String,
        label: "Id invoice",
        max: 200
    },
    transactionId: {
        type: Number,
        label: "Transaction id",
    },
    sum: {
        type: Number,
        label: "Sum",
        decimal: true
    },
    status: {
        type: String,
        label: "Status",
        allowedValues: ['init', 'done']
    },
    message: {
        type: String,
        label: "Message",
        max: 200
    },
});

payments.attachSchema(schemas.payments);

//(id:string, id_ServiceRequest: string, id_Client:string, id_Handyman:string, countMessages:Int)
schemas.chat = new SimpleSchema({
    id_Client: {
        type: String,
        label: "Id client",
        max: 200
    },
    id_Handyman: {
        type: String,
        label: "Id handyman",
        max: 200
    },
    countMessages: {
        type: Number,
        label: "Count messages",
        defaultValue: 0,
        optional: true
    },
    dateLastMessage: {
        type: Date,
        label: "Date of last message",
        defaultValue: 0,
        optional: true
    }
});

chat.attachSchema(schemas.chat);

//(id_Chat:string, message:text, from:id_Client|id_Handyman, date:Int)
schemas.chat_messages = new SimpleSchema({
    id_Chat: {
        type: String,
        label: "Id chat",
        max: 200
    },
    message: {
        type: String,
        label: "Message",
        max: 1000
    },
    from: {
        type: String,
        label: "Id author",
        max: 200
    },
    date: {
        type: Date,
        label: "Date",
    },
});

chat_messages.attachSchema(schemas.chat_messages);

//(id_ServiceRequest:String, message:String, rating:Int[0,5])
schemas.feedbacks = new SimpleSchema({
    id_ServiceRequest: {
      type: String,
      label: "Id service request",
      max: 200
    },
    message: {
        type: String,
        label: "Message",
        max: 1000
    },
    rating: {
        type: Number,
        label: "Rating",
        min:0,
        max:5
    },
});

feedbacks.attachSchema(schemas.feedbacks);

schemas.contact = new SimpleSchema({
    name: {
        type: String,
        label: "Your name",
        max: 50,
        defaultValue: function() {
          if(Meteor.user()) {
            var _handyman_profile = handyman_profile.findOne({"id_Handyman": Meteor.userId()});
            return ( !_.isUndefined(_handyman_profile) && !_.isUndefined(_handyman_profile.fullName) ) ? _handyman_profile.fullName : "";
          }
        }
    },
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: "E-mail address",
        defaultValue: function() {
          if(Meteor.user()) {
            return Meteor.user().emails[0]['address'];
          }
        }
    },
    phone: {
        type: String,
        regEx: /^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]{3,25}$/,
        label: "Phone",
        optional: true,
        defaultValue: function() {
          if(Meteor.user()) {
            var _handyman_profile = handyman_profile.findOne({"id_Handyman": Meteor.userId()});
            return ( !_.isUndefined(_handyman_profile) && !_.isUndefined(_handyman_profile.phone) ) ? _handyman_profile.phone : "";
          }
        }
    },
    message: {
        type: String,
        label: "Message",
        max: 1000
    }
});

schemas.notification_history = new SimpleSchema({
    id_User: {
        type: String,
        label: "Id user",
        max: 200
    },
    from: {
        type: String,
        label: "Name of sender",
        max: 200
    },
    path: {
        type: String,
        label: "Path",
        optional: true,
        max: 200
    },
    pathId: {
        type: String,
        label: "Path id",
        optional: true,
        max: 200
    },
    message: {
        type: String,
        label: "Message",
        max: 1000
    },
    created: {
        type: Date,
        label: "Date of creating",
        optional: true,
        autoValue: function() {
            return new Date;
        }
    },
});

notification_history.attachSchema(schemas.notification_history);
