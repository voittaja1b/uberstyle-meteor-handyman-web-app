Template.ApplicationLayoutDynamic.helpers({
  showLeftMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showLeftMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  showSubNavBar: function() {
    return Router.current().route.getName() === "HandymanMap";
  },
  toolbarMap: function() {
    return Router.current().route.getName() === "HandymanMap";
  },
  toolbarMessage: function() {
    return Router.current().route.getName() === "Chat";
  },
  historyBack: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return Session.get("previousLocationPath") != null ? true : false;
    }
  }
});

var attachNotification = function() {
  notification_history.find().observe({
    added: function (doc) {
      var link = "";
      if((Router.current().route.getName() == doc.path && _.isUndefined(doc.pathId)) ||
         (Router.current().route.getName() == doc.path && !_.isUndefined(doc.pathId) && Router.current().params._id == doc.pathId)) {
           Meteor.call("deleteNotification", doc._id);
        return;
      } else {
        _.map(Router.routes, function(func){
          if(func.getName() == doc.path) {
            link = func.options.path;
          }
        });
        if(!_.isUndefined(doc.pathId)) {
          link = new String(link).replace(":_id", doc.pathId);
        }
      }
      var message = "<a href='" + link + "'>" + (!_.isUndefined(doc.from) ? "<b>" + doc.from + ":</b>" : "") + " " + doc.message  + "</a>";
      if (Meteor.isCordova) {

      } else {
        if(Platform.isWeb() || Session.get('platformOverride') === 'web') {
          bootstrap_alert.notification(doc._id, message);
        }
        if(Platform.isAndroid() || Session.get('platformOverride') === 'android') {
          myApp7.addNotification({
              message: message,
              hold: 5000,
              button: {
                  text: 'Close',
                  color: 'lightgreen'
              },
              onClose: function () {
                  Meteor.call("deleteNotification", doc._id);
              }
          });
        }
        if(Platform.isIOS() || Session.get('platformOverride') === 'ios') {
          myApp7.addNotification({
              title: message,
              message: 'Click icon to close',
              hold: 5000,
              onClose: function () {
                  Meteor.call("deleteNotification", doc._id);
              }
          });
        }
      }
    },
    changed: function (doc) {

    },
    removed: function (doc) {
      if (Meteor.isCordova) {

      } else {
        if(Platform.isWeb) {
          bootstrap_alert.close(doc._id);
        }else {

        }
      }
    }
  });
};

//Init Framework7
Template.ApplicationLayoutDynamic.onRendered(function() {

    // Initialize app on server and on client
    myApp7 = new Framework7({
        router: false,
        //swipePanel: 'left',
        swipeBackPage: true,
        animatePages: true
    });

    // If we need to use custom DOM library, let's save it to $$ variable:
    $$ = Dom7;

    mainView = myApp7.addView('.view-main', {
        onSwipeBackBeforeChange: function(callbackData) {
            history.back();
        }
    });

    this.find('.pages')._uihooks = {
        insertElement: function(node, next) {
            mainView.router.loadContent(node);
        },
        removeElement: function(node) {
            return false;
        }
    };

    //prevent show panels
    $("body").removeClass("with-panel-left-reveal").removeClass("with-panel-right-reveal");

    //Notification
    attachNotification();
});

Template.web_ApplicationLayoutDynamic.onRendered(function() {
  var platform = 'platform-web';
  if ((Meteor.isCordova && Platform.isIOS() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'ios') {
    platform = 'platform-ios';
  }
  if ((Meteor.isCordova && Platform.isAndroid() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'android') {
    platform = 'platform-android';
  }
  $("body").addClass(platform);

  //Notification
  attachNotification();
});

Template.AccountsLayout.onRendered(function() {

    // Initialize app on server and on client
    myApp7 = new Framework7({
        router: false,
        //swipePanel: 'left',
        swipeBackPage: true,
        animatePages: true
    });

    // If we need to use custom DOM library, let's save it to $$ variable:
    $$ = Dom7;

    mainView = myApp7.addView('.view-main', {
        onSwipeBackBeforeChange: function(callbackData) {
            history.back();
        }
    });

    this.find('.pages')._uihooks = {
        insertElement: function(node, next) {
            mainView.router.loadContent(node);
        },
        removeElement: function(node) {
            return false;
        }
    };

    //prevent show panels
    $("body").removeClass("with-panel-left-reveal").removeClass("with-panel-right-reveal");
});

Template.ApplicationLayoutDynamic.events({
  "click #historyBack": function() {
    Router.go(Session.get("previousLocationPath"));
    return false;
  },
  "click #getMyLocation": function(event, template) {
    var marker = {
      _id: "i",
      transform: function(marker) {
        return marker;
      },
      onClick: function (event) {
        console.log(event);
      },
      icon: '/icon/user_location.png',
      // This marker is 20 pixels wide by 32 pixels high.
      size: new google.maps.Size(32, 32),
      // The origin for this image is (0, 0).
      origin: new google.maps.Point(0, 0),
      // The anchor for this image is the base of the flagpole at (0, 32).
      anchor: new google.maps.Point(0, 32),
      infoWindowAnchor: [0,0]
    };
    //Here we just watch for reactive data and change location for marker and center for Map
    Tracker.autorun(function () {
      var location = Geolocation.currentLocation();
      var error = Geolocation.error();
      if(error) {
        switch (error.code) {
          case error.PERMISSION_DENIED:
              myApp7.alert("Device denied the request for Geolocation", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          case error.POSITION_UNAVAILABLE:
              myApp7.alert("Location information is unavailable.", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          case error.TIMEOUT:
              myApp7.alert("The request to get device location timed out.", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          case error.UNKNOWN_ERROR:
              myApp7.alert("An unknown error occurred.", "Ups, something happened");
              adaptiveMap.removeMarker(marker);
              break;
          default:
            break;
        }
      }

      if(!location) {
        return;
      }
      Meteor.call("updateHandymanLocation", {"lat": location.coords.latitude, "lng": location.coords.longitude});
      adaptiveMap.setCenter(new AdaptiveMaps.LatLng(location.coords.latitude, location.coords.longitude));
      marker.position = new AdaptiveMaps.LatLng(location.coords.latitude, location.coords.longitude);

      if(adaptiveMap._markers["i"]) {
        adaptiveMap.changeMarker(marker);
      } else {
        adaptiveMap.addMarker(marker);
      }
    });

    //restrict of redirection
    return false;
  },
  "typeWatch #getAddressGeocoder": function(event, template) {
      var address = template.find("#getAddressGeocoder").value;
      // Just we don't allow address less then 3 symbols
      if(address.length < 4) {
        return true;
      }
      Meteor.call("locationByAddress", address, function(error, result){
        if(error) {
          myApp7.alert("Sorry, we cannot determine a location by this address.", "Ups, something happened");
          //@TODO remove this
          if(typeof(Meteor.settings.public.debug) != "undefined" && Meteor.settings.public.debug) {
              console.log("error", error);
          }
        }
        if(result && result.length > 0) {
          //result[0].formattedAddress:"Khod, Rajasthan 306119, India"
          adaptiveMap.setCenter(new AdaptiveMaps.LatLng(result[0].latitude, result[0].longitude));

          //@TODO carry out in other extended Geocoder api file
          var zoom = 12;
          var tabZoom =  {
              streetNumber: 20,
              streetName: 15,
              zipcode: 13,
              city: 10,
              country: 5
          };
          for(var i in tabZoom) {
            if (typeof(result[0][i]) != "undefined"){
              zoom = tabZoom[i];
              break;
            }
          }
          adaptiveMap.setZoom(zoom);
        }
      });
  },
  "click #sendMessage": function(event, t){
      var message = $("#bodyMessage").val();
      if(message) {
        $("#bodyMessage").val('');
        Meteor.call("sendMessageToChat", {"message" : message, "id_Chat": $("#chat_id").val()},function(error, result) {
          if(error) {
            console.log(error);
          }
          var height = $(".messages").outerHeight();
          $(".page-content").stop().animate({scrollTop: height}, '500', 'swing');
        });
      }
  },
  "refresh": function(event) {
    var message_count = $("#message_count").val();
    Tracker.autorun(function () {
      var _chat_subscribe = Meteor.subscribe('chatHistory', $("#chat_id").val(), parseInt(message_count) + 50);
      Session.set("loadingHistory", true);
      if (_chat_subscribe.ready()) {
        myApp7.pullToRefreshDone();
        setTimeout(function() {
          Session.set("loadingHistory", false);
        }, 500);
        $("#message_count").val(parseInt(message_count) + 50);
      }
    });
  }
});

Template.extendOnPlatforms(['android','ios','web'], 'ApplicationLayoutDynamic');

Template.web_ApplicationLayoutDynamic.helpers({
  showLeftMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showLeftMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenu: function() {
    if(!Meteor.userId()) {
        return false;
    } else {
        return true;
    }
  },
  showRightMenuIcon: function() {
    if(!Meteor.userId() || ((Meteor.isClient && Platform.isWeb() && !Session.get('platformOverride')) || Session.get('platformOverride') === 'web')) {
        return false;
    } else {
        return true;
    }
  },
  Title: function() {
    return false;
  },
  showSubNavBar: function() {
    return true;
  },
  toolbarMap: function() {
    return true;
  },
  showTitle: function() {
    return _.indexOf(["Chat"], Router.current().route.getName() ) === -1;
  },
  historyBack: function() {
    return false;
  }
});
